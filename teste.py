#-*- encoding: utf-8 -*-
from correios import Correios
try:
    arq = open("pacotes")
    lido = arq.read()
except:
    print "Erro ao ler arquivo"

pacotes = lido.split(';')
print len(pacotes)
for pacote in pacotes:
    dados = pacote.split(':')
    if len(dados) != 3:
        print 'Linha fora do padrão'
        continue
    print '==========================='
    print 'Descricao: ' + str(dados[0])
    print 'Tipo: ' + str(dados[1])
    print '---------------------------'
    try:
        encomenda = Correios.get_encomenda(dados[2])
    except Exception, resp:
        print "Não encontrou pacote " + str(dados[2])
        print resp
        continue
        
    print 'Encomenda:', encomenda.identificador
    for status in encomenda.status:
        print 'Data da atualização:', status.atualizacao.date()
        print 'Hora da atualização:', status.atualizacao.time()
        print 'País:', status.pais
        print 'Estado:', status.estado
        print 'Cidade:', status.cidade
        print 'Agência:', status.agencia
        print 'Situação:', status.situacao
        print 'Observação:', status.observacao
        print

