#-*- encoding: utf-8 -*-
from correios import Correios
import datetime
from datetime import timedelta
try:
    arq = open("pacotes")
    lido = arq.read()
except:
    print "Erro ao ler arquivo"

pacotes = lido.split(';')
print str(len(pacotes)) + " pacotes listados."
for pacote in pacotes:
    dados = pacote.split(':')
    if len(dados) < 3:
        print 'Linha fora do padrão'
        continue
    print '==========================='
    print 'Descricao: ' + str(dados[0])
    print 'Tipo: ' + str(dados[1])
    if len(dados)==4:
        split_data = dados[3].split('.')
        if len(split_data) != 6:
            print "data no formato errado!"
            continue
        datafinal = datetime.datetime(int(split_data[2]), int(split_data[1]), int(split_data[0]),int(split_data[3]), int(split_data[4])) +timedelta(days=int(split_data[5]))+timedelta(hours=4) 

        print 'Tempo restante: '+str(datafinal - datetime.datetime.now())
    print '---------------------------'
    try:
        encomenda = Correios.get_encomenda(dados[2])
    except Exception, resp:
        print "Problema com o pacote " + str(dados[2]) + ": " + str(resp)
        continue
        
    print 'Encomenda:', encomenda.identificador
    status = encomenda.status[-1]
    print 'Data da atualização:', status.atualizacao.date()
    print 'Hora da atualização:', status.atualizacao.time()
    print 'País:', status.pais
    print 'Estado:', status.estado
    print 'Cidade:', status.cidade
    print 'Agência:', status.agencia
    print 'Situação:', status.situacao
    print 'Observação:', status.observacao
    print ''

