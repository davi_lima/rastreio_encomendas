#-*- encoding: utf-8 -*-
from correios import Correios
import datetime
from datetime import timedelta
from correios import Encomenda
import operator
try:
    arq = open("pacotes")
    lido = arq.read()
except:
    print "Erro ao ler arquivo"

#controle de erro nas linhas
linhas_fora_padrao = []
num_linha = 0

encomendas = []

pacotes = lido.split(';')
for pacote in pacotes:
    num_linha +=1
    dados = pacote.split(':')
    print dados
    if len(dados) != 4:
        if len(dados) == 1:
            if dados[0] == '\n':
                continue
        print "problema no split da linha. len(dados) " + str(len(dados))
        linhas_fora_padrao.append(num_linha)
        continue
    split_data = dados[3].split('.')
    if len(split_data) != 6:
        print "problema no split da data. len(split_data) " + str(len(split_data))
        linhas_fora_padrao.append(num_linha)
        continue
    encomenda = Encomenda(dados[2])
    encomenda.descricao = dados[0].replace('\n','')
    encomenda.tipo = dados[1]
    encomenda.dataEnvio = datetime.datetime(int(split_data[2]),int(split_data[1]),int(split_data[0]),int(split_data[3]),int(split_data[4]))
    encomenda.dataPrevista = encomenda.dataEnvio + timedelta(days=int(split_data[5]))+timedelta(hours=4)

    try:
        encomenda_status = Correios.get_encomenda(encomenda.identificador)
    except Exception, resp:
        print "Excecao: " +str(resp)
        encomenda.rastreio = str(resp)
        encomendas.append(encomenda)
        continue
    encomenda.status = encomenda_status.status
    encomendas.append(encomenda)
        
#ordena por data prevista -_-
encomendas.sort(key=operator.attrgetter('dataPrevista'))

for encomenda in encomendas:
    
    print '=============================================================================================================================='
    print '------------------------------------------------------------------------------------------------------------------------------'
    print 'Descricao: ' +str(encomenda.descricao)
    #print 'Data de envio: '+str(encomenda.dataEnvio)
    #print 'Data de Prevista: '+str(encomenda.dataPrevista)
    print '********* TEMPO RESTANTE: '+str(encomenda.dataPrevista - datetime.datetime.now())+' **********'
    print 'Encomenda:', encomenda.identificador
    try:
        status = encomenda.status[-1]
    except:
        print 'Rastreio: '+ str(encomenda.rastreio)
        print '------------------------------------------------------------------------------------------------------------------------------'
        print '=============================================================================================================================='
        continue
    print 'Data da atualização:'+ str(status.atualizacao.date()) + " " + str(status.atualizacao.time())
    print 'País:', status.pais
    print 'Estado:', status.estado
    print 'Cidade:', status.cidade
    print 'Agência:', status.agencia
    print 'Situação:', status.situacao
    print 'Observação:', status.observacao
    print '------------------------------------------------------------------------------------------------------------------------------'
    print '=============================================================================================================================='
    print ''

if(len(linhas_fora_padrao)>0):
    print 'As linhas: ' +str(linhas_fora_padrao) + " estao fora do padrao" 
